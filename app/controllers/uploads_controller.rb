class UploadsController < ApplicationController

  skip_before_action :verify_authenticity_token

  def create
    filename = params[:uploaded_file]
    return miss_params_response if filename.blank?

    s = Storage.create(filename: filename)

    @response = {stat: "ok", file_url: "http://#{request.host_with_port}#{s.filename}"}
    respond_to do |format|
      format.json { render :json => @response }
    end
  end

end
